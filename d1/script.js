console.log("*** JS Objects ***")

/*
    Javascript Objects
    
    A data type that is used to represent real world objects

    - Object literals {}
    - Information are stored in key: value pair
    - Key = properties
    
    In JS, most core JS Features like Strings and arrays are objects
    - Arrays are collections of data
    - Strings are collection of characters
    - Different data types maybe stored in an object's property
        creating complex data structures
*/

let gradesArray = [98, 94, 89, 90]  

let grades = {
    firstGrading: 98,
    secondGrading: 94,
    thirdGrading: 89,
    fourthGrading: 90
}
console.log(grades)

let user = {
    firstName: 'John',
    lastName: 'Doe',
    age: 25,
    location: {
        city: 'Tokyo',
        country: 'Japan'
    },
    emails: [
        'john@mail.com',
        'johndoe@mail.com'
    ],
    fullName: function() {
        return this.firstname + " " + this.lastName;
    }
}
console.log(user)
console.log(user.fullName())

// Creating Objects
// 1) Using Object initializers/literal notation

/* *This* creates/declarees an object and also
initializes/ assigns its properties upon creation

A cellphone is an example of a real world object;
It has its own properties such as name, color, weight, unit model, etc.

Syntax:
    let objectName = }
    keyA: valueA,
    keyB: valueB
}
*/

let cellphone = {
    name: 'Nokia 3210',
    manufacturedDate: 1999
}
console.log(`Result from using initializers: ${cellphone}`)
console.log(cellphone)
console.log(typeof cellphone)

/* 2) Using constructor function
        - creates a reusable function to create several objects taht have the 
            same data structure.
        - this useful for creating multiple instances/copies of an object

    SYNTAX:
        function ObjectName(keyA, keyb) {
            this.keyA = keyA
            this.keyB = keyb
        }

    let newObject = new ObjectName(keyA, keyB)
*/
function Laptop (name, manufactureDate, color) {
    this.name = name
    this.manufacturedDate = manufactureDate
    this.color = color
}

let laptop = new Laptop('Lenovo', 2008)
console.log(`Result: from using object constructors: ${laptop}`)
console.log(laptop)

let myLaptop = new Laptop('Macbook Air', 2020, 'space grey')
console.log(`Result from using object constructors: ${myLaptop}`)
console.log(myLaptop)

let oldLaptop = Laptop('Portal r2E CCMC', 1980)
console.log(`Result without the 'new' keyword:`)
console.log(oldLaptop) // This will result in undefined

// Creating empty object
let computer = {}
let myComputer = new Object()

// Accessing Object Properties

// 1) Using the dot notation
console.log(`Result from dot notation: ${myLaptop}`)
console.log(myLaptop.name)

// 2) Using 
console.log(`Result from square bracket: `, myLaptop['name'])

let array = [laptop, myLaptop]

console.log(array[0]['name'])
console.log(array[0].name) // best practice (dot notation)

// Initializing Object Properties

let car = {}

car.name = "Honda Civic"
console.log(`Result from adding dot notation:`, car)

car['Manufactured Date'] = 2019
console.log(car['Manufactured Date'])
console.log(`Result from using square bracket notation`, car)

// Deleting Object properties

delete car['Manufactured Date']
console.log(`Result from deleting properties`, car)

// Reassigning properties

car.name = `Dodge Charger R/T`
console.log(`Result from reassigning properties: `, car)

// Object Methods

/*
A method is a function which is a property of an object
Similar to functions/featurese of a real world objects, methods
are defined based on what an object is capable of doing and how it hsould work


*/

let person = {
    name: 'John',
    talk: function() {
        console.log(`Hello my name is ${this.name}`)
    }
}
console.log(person)
console.log(`Result from object method`)
person.talk()

person.walk = function() {
    console.log(this.name, `walked 25 steps forward.`)
}
person.walk()

let friend = {
    firstName: 'Joe',
    lastName: 'Smith',
    address: {
        city: 'Austing',
        state: 'Texas',
        country: 'United States of America'
    },
    emails: [
        'joe@mail.com',
        'joesmith@mail.com'
    ],
    introduce: function() {
        console.log(`Hello, my name is: ${this.firstName} ${this.lastName}`)
    }
}
friend.introduce()
console.log(friend.emails[0])

/*
Real World Applicaiton of Objects
- Scenario
1) We would like to create a game taht would have
several pokemon intereact with
each other
2) Every pokemon would have the same
set of stat, properties, and functions
*/

// Using object literals to create multiple kinds of pokemon would be time consuming

let myPokemon = {
    name: 'Pikachu',
    level: 3,
    health: 100,
    attack: 50,
    tackle: function() {
        console.log(`This Pokemon tackled targetPokemon`)
    },
    faint: function() {
        console.log(`Pokemon fainted.`)
    }
}
console.log(myPokemon)

// Better to use constructor if you're gonna create a lot of objects

function Pokemon(name, level) {
    // Properties
    this.name = name
    this.level = level
    this.health = 2 * level
    this.attack = level

    // Methods
    this.tackle = function(target) {
        console.log(`${this.name} tackled ${target.name}`)
        console.log(`${target.name}'s health is now reduced to _targetPokemonHealth_`)
    }
    this.faint = function() {
        console.log(`${this.name} fainted.`)
    }
}
let pikachu = new Pokemon('Pikachu', 16)
let rattata = new Pokemon('Rattata', 8)

pikachu.tackle(rattata)